<?php

use Entity\Usuario;

if (!defined('BASEPATH')) {
    exit('No direct script access allowed.');
}

class MY_Session extends CI_Session {

    private $ci;

    public function __construct() {
        // en vez de usar $this-> vamos a usar $this->ci->
        session_start();
        $this->ci = & get_instance();
    }


    public function cerrar_sesion() {
        $this->sess_destroy();
        redirect('login/index');
    }

    public function iniciar_sesion() {

        $response = [
            'success' => FALSE, // salvo que se cambie mas adelante , se retornara falso el success
            'errorMsg' => ''
        ];
        // guardamos en una variable php lo que ingreso el usuario 
        $email = $this->ci->input->post('usuario');
        $password = $this->ci->input->post('password');
        // buscamos un usuario con ese mail
        $this->ci->load->model('usuario');
        // si el usuario ingrso el mail
        if ($email) {
            // buscamos en la tabla usuarios que haya algun mail que coincida con el ingresado
            $usuario = $this->ci->usuario->findBy(array('email' => $email));
            // si el usuario ingreso password
            if ($password && $usuario) {
                // obtenemos el password del usuario encontrado en la tabla usuarios en el paso anterior, y lo comparamos con el email ingresado por el usuario
                if ($usuario[0]->password == $password) {
                    // si coincide el mail ingresado con el password almacenado para ese mail, devolvemos true en la respuesta, cambiamos el valor de success
                    //  la unica forma de que devuelva success == true es que se entre en esta condicion
                    $response['success'] = TRUE;
                    $this->ci->load->model('perfil');
                    $perfil_desc = $this->ci->perfil->find($usuario[0]->id_perfil)->rol;
                    // guardamos el id_usuario, el id_perfil y el mail, en las variables de sesion para tener disponibles en toda la aplicacion
                    $this->set_userdata('id_usuario', $usuario[0]->id);
                    $this->set_userdata('id_perfil', $usuario[0]->id_perfil);
                    $this->set_userdata('email', $usuario[0]->email);
                    $this->set_userdata('perfildesc', $perfil_desc);
                    return $response;
                } else {// si no coincide el password ingresado con el password almacenado en bbdd para el mail ingresado 
                    $response['errorMsg'] = "DATOS INCORRECTOS, MAIL O PASSWORD INVALIDOS";
                }
            } else {// si no se ingreso el password
                $response['errorMsg'] = "DATOS INCORRECTOS, MAIL O PASSWORD INVALIDOS";
            }
        } else {// si no se ingreso el email
            $response['errorMsg'] = "DATOS INCORRECTOS, MAIL O PASSWORD INVALIDOS";
        }
        // se retorna la respuesta
       
        // *Si no se ingreso password o email, o si no coincide el usuario y el mail con los datos de nuestra bbdd, se devolvera :
        // "success = false" y "errorMsg = DATOS INCORRECTOS, MAIL O PASSWORD INVALIDOS"
        
        // *Si se ingreso password y mail, y estos son correctos, es decir coinciden con los datos de nuestra bbdd, se devolvera :
        // "success = true" y errorMsg = ' ' (string vacio)
        return $response;
    }

}
