
<script src="http://localhost:8080/animalsfood/assets/js/home/home.js" type="text/javascript"></script>
<?php
// guardamos los datos del usuario logueado en variables, para armar el menu segun el usuario
$usuario = array('usuario' => $this->session->userdata('id_usuario'),
                         'perfil' => $this->session->userdata('id_perfil'),
                         'email' => $this->session->userdata('email')
                         );
 
 $this->load->view('home/menu', $usuario);
 ?>
<section id="section-ofrecemos" class="container-fluid fixed">
    <header >
        <h1 class="title-home">Nuestros Servicios</h1>
        <hr>
        <!--p class="subtitle text-center">Puedes comprar alimentos </p-->
    </header>
    <div class="contain-flex ">

        <article class="text-center ">
            <h3>
                Alimentos
            </h3>
            <p>
                Compre el mejor alimento balanceado para la bestia de su mascota.
            </p>
            <figure>
                <img src="http://localhost:8080/animalsfood/assets/img/food.jpg">
            </figure>
        </article>

        <article class="text-center  ">
            <h3 class="title-gift">
                Salud
            </h3>
            <p>
                Vacune y drogue a sus perros en el mejor lugar.
            </p>
            <figure>
                <img src="http://localhost:8080/animalsfood/assets/img/dog.jpg">
            </figure>
        </article>

        <article class="text-center ">
            <h3>
                Cuchas
            </h3>
            <p>
                Compre el mejor refugio para el imbecil de su perro.
            </p>
            <figure>
                <img src="http://localhost:8080/animalsfood/assets/img/house.jpg">
            </figure>
        </article>
        
    </div>
</section>


<section id="nosotros" class="container-fluid fixed ">
    <header>
        <h1 class="text-center titles title-home">Nosotros</h1>
        <p class=" supersubtitle text-center">Somos un grupo de drogadictos en recuperacion que montamos una perrera.<br> Brindamos las mejores dosificaciones a su mascota y se la entregamos como una ceda</p>
    </header>
    <div class="contain-flex nos ">
        <div class="text-center margin-left ">
            <h2>
                Vendemos carne de mono
            </h2>
            <p>La trituramos y mezclamos con ceso de iguana para lograr un mejor efecto</p>
        </div>
        <div class="text-center margin-left ">
                <h2>
                    Cuidamos a tu mascota
                </h2>
                <p>Los tenemos encerrados mientras te vas de vacaciones, los dejas en las mejores manos</p>
        </div>
    </div>
</section>




