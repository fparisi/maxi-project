
<?php if (!$usuario): // si no esta el usuario logueado        ?>
    <div class="alert alert-danger text-center" id="sinpermiso">TIENE QUE ESTAR LOGUEADO PARA ACCEDER AL SISTEMA, INGRESE DESDE <a href="http://localhost:8080/animalsfood">AQUI</a></div>
<?php else: ?>  
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ">
                    <li><a href="http://localhost:8080/animalsfood/home/">Inicio</a></li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <!--  solo se mostrara la opcion de agreagr productos si el perfil es administrador  -->
                            <?php if ($perfil == 1): ?>
                                <li>
                                    <a href="http://localhost:8080/animalsfood/productos/alta">
                                        Agregar 
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="http://localhost:8080/animalsfood/productos/consultar">
                                    <?php if ($perfil == 2): ?>
                                        Comprar 
                                    <?php elseif ($perfil == 1): ?>
                                        Consultar 
                                    <?php endif; ?>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mi cuenta<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <?php if ($perfil == 2): ?>
                                    <a href="http://localhost:8080/animalsfood/transacciones/alta">Mis compras</a>
                                <?php else: ?>
                                    <a href="http://localhost:8080/animalsfood/transacciones/alta">Ver Movimientos</a>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </li>

                    <?php if ($perfil == 1): ?>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="http://localhost:8080/animalsfood/usuarios/alta" >
                                        Agregar 
                                    </a>
                                </li>
                                <li>
                                    <a href="http://localhost:8080/animalsfood/usuarios/modificar" >
                                        Modificar
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="http://localhost:8080/animalsfood/login/salir">
                            Log out
                        </a>
                    </li> 
                    <li>
                        <a href="#">
                            <p><b><?php echo $email; ?></b></p>
                        </a>
                    </li> 
                    <li>
                        <a href="#">
                            <p><b>Perfil: <?php echo $this->session->userdata('perfildesc'); ?></b></p>
                        </a>
                    </li> 
                </ul> 
            </div><!-- /.navbar-collapse -->

        </div><!-- /.container-fluid -->
    </nav>
<?php endif; ?>