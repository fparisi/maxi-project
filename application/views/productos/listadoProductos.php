<table class="table table-striped">
  <?php  if($productos): ?>
    <thead>
        <tr>
            <th  class="hidden">id</th>
            <th>Descripcion</th>
            <th>Marca</th>
            <th>Precio</th>
            <?php if($perfil == 1): ?>
            <th>Stock</th>
            <?php endif; ?>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
      <?php foreach ($productos as $producto): ?>
        <tr>
            <td class="hidden"><?php echo $producto->id?></td>
            <td><?php  echo $producto->producto  ?></td>
            <td><?php  echo $producto->marca  ?></td>
            <td><?php  echo $producto->precio   ?></td>
             <?php if($perfil == 1): ?>
            <td><?php  echo $producto->stock ?></td>
            <?php endif; ?>
            <?php if($this->session->userdata('id_perfil') == 2): ?>
            <td><input id="cantidad" type="number" placeholder="cantidad" class="form-control"></td>
            <td><a data-action="comprar-producto" class="label label-success">Comprar</a></td>
            <?php elseif($this->session->userdata('id_perfil') == 1): ?>
            <td><a data-action="eliminar-producto" class="label label-danger">Eliminar</a></td>
            <?php endif; ?>
        </tr>
       <?php endforeach; ?>
    </tbody>
    <?php else: ?>
    <p class="alert alert-warning text-center">No hay resultados para la bùsqueda</p>
    <?php  endif; ?>
</table>