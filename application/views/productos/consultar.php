<script src="http://localhost:8080/animalsfood/assets/js/productos/consultar.js" type="text/javascript"></script>
<?php
$this->load->view('home/menu');
?>
<?php if ($usuario):?>
<div class="container-fluid">
    <h1>Consulta de Productos</h1>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel panel-heading"> 
                    <h3 class="panel-title">Filtros Consulta</h3>
                </div>
                <div class="panel panel-body">
                    <?php echo form_open(site_url('productos/consultar_producto'), array('id' => 'frm-producto-consultar')) ?>
                    <div class="form-group">
                        <label>Descripcion</label>
                        <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Ingrese la descripcion del producto">
                        <span class="help-block"></span>
                    </div>
                    <button type="submit" id="consultarTodos" class="btn btn-primary">Consultar</button>
                    <button type="reset" class="btn btn-primary">Limpiar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default hidden" id="panelListadoProductos" >
                <div class="panel panel-heading">
                    <h3 class="panel-title">Listado de productos</h3>
                </div>
                <div class="panel panel-body"  id="divListadoConsulta">
                    <!-- se carga la vista con el listado, por la funcion $.load -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
