
<script src="http://localhost:8080/animalsfood/assets/js/productos/alta.js" type="text/javascript"></script>
<?php
 $this->load->view('home/menu');
?>
<?php if ($usuario):?>
<div class="container-fluid">
        <h1>Alta Producto</h1>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos producto</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open('productos/registrar_producto',array('id' => 'frm-productos-alta')) ?>
                        <div class="form-group">
                            <label>Descripcion</label>
                            <input type="text" name="descripcion" id="descripcion" class="form-control" placeholder="Ingrese la descripcion del producto">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Marca</label>
                            <select name="marca"  id="marca" class="form-control">
                                <option value="">--SELECCIONE--</option>
                                 <?php foreach ($marcas as $marca): ?>
                                <option value="<?php  echo $marca->id ?>"><?php  echo $marca->nombre ?></option>
                                 <?php endforeach; ?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Stock</label>
                            <input type="text" name="stock" id="stock" class="form-control" placeholder="Ingrese la cantidad que hay en stock">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Precio</label>
                            <input type="text" name="precio" id="precio" class="form-control" placeholder="Ingrese el valor del producto">
                            <span class="help-block"></span>
                        </div>
                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <button type="reset" class="btn btn-primary">Limpiar</button>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>