
<script src="http://localhost:8080/animalsfood/assets/js/usuarios/alta.js" type="text/javascript"></script>
<?php
 $this->load->view('home/menu');
?>

<div class="container-fluid">
        <h1>Alta Usuario</h1>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos usuario</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open('productos/registrar_usuario',array('id' => 'frm-usuarios-alta')) ?>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Ingrese su nombre">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" id="apellido" class="form-control" placeholder="Ingrese su apellido">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>DNI</label>
                            <input type="text" name="dni" id="dni" class="form-control" placeholder="Ingrese su dni">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Ingrese su telefono">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>Domicilio</label>
                            <input type="text" name="domicilio" id="domicilio" class="form-control" placeholder="Ingrese su domicilio">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="Ingrese su email">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name="password" id="password" class="form-control" placeholder="Ingrese su password">
                            <span class="help-block"></span>
                        </div>
                        <?php if($perfil == 1):?>
                        <div class="form-group">
                            <label>Perfil</label>
                            <select name="perfil"  id="perfil" class="form-control">
                                <option value="">--SELECCIONE--</option>
                                 <?php foreach ($perfiles as $perfil): ?>
                                <option value="<?php  echo $perfil->id ?>"><?php  echo $perfil->rol ?></option>
                                 <?php endforeach; ?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                        <?php endif; ?>

                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <button type="reset" class="btn btn-primary">Limpiar</button>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
