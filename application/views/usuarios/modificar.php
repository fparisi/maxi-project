<script src="http://localhost:8080/animalsfood/assets/js/usuarios/modificar.js" type="text/javascript"></script>
<?php
 $this->load->view('home/menu');
?>

<div class="container-fluid">
    <?php if($usuario_actual): ?>
        <h1>Modificar Usuario</h1>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos usuario</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo form_open('productos/modificar_usuario',array('id' => 'frm-usuarios-modificar')) ?>
                            <input type="hidden" id="id_user" class="" value="<?php echo $usuario_actual->id; ?>" >
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" value="<?php echo $usuario_actual->nombre; ?>" name="nombre" id="nombre" class="form-control" placeholder="Ingrese su nombre">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" value="<?php echo $usuario_actual->apellido; ?>" id="apellido" class="form-control" placeholder="Ingrese su apellido">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>DNI</label>
                            <input type="text" name="dni" value="<?php echo $usuario_actual->dni; ?>" id="dni" class="form-control" placeholder="Ingrese su dni">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>Telefono</label>
                            <input type="text" name="telefono"  value="<?php echo $usuario_actual->telefono; ?>" id="telefono" class="form-control" placeholder="Ingrese su telefono">
                            <span class="help-block"></span>
                        </div>
                         <div class="form-group">
                            <label>Domicilio</label>
                            <input type="text" name="domicilio" value="<?php echo $usuario_actual->domicilio; ?>" id="domicilio" class="form-control" placeholder="Ingrese su domicilio">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" value="<?php echo $usuario_actual->email ;?>" id="email" class="form-control" placeholder="Ingrese su email">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name="password" id="password" class="form-control" placeholder="Deje el campo vacio para conservar su password actual ">
                            <span class="help-block"></span>
                        </div>
                       
                       

                        <button type="submit" class="btn btn-primary">Registrar</button>
                        <button type="reset" class="btn btn-primary">Limpiar</button>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
        <?php else: ?>
        <div class="alert alert-danger">No hay un usuario logueado, no puede mostrarse el formulario</div>
        <?php endif; ?>
    </div>
