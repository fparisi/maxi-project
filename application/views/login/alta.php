<link  rel="stylesheet" href="http://localhost:8080/animalsfood/assets/css/login.css">
<script src="http://localhost:8080/animalsfood/assets/js/login/login.js"></script>
<?php ?>
<div class="container-fluid">
    <div class='row' id="containerLogin">
        <div class="col-md-3 " >
            <h1 class="hidden">Sistema de ventas</h1>
            <div class="form-signin">
                <?php echo form_open("http://localhost:8080/animalsfood/login/iniciar", array('id' => 'frm-iniciar-sesion')) ?>
                <h2 class="form-signin-heading">Ingresar al sistema</h2>
                <div class="form-group">
                    <label  class="sr-only">Email </label>
                    <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Escriba su direccion de correo" autofocus>
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label  class="sr-only">Password</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                    <span class="help-block"></span>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit" >Ingresar</button>
                <div id="msg"><span class="help-block "></span></div>
                <div><p><a href="http://localhost:8080/animalsfood/usuarios/alta">Registrate</a></p></div>
                <?php echo form_close(); ?> 
            </div>
        </div>
    </div>
</div>

