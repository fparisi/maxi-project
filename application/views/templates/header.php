<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Animals</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://localhost:8080/animalsfood/assets/lib/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/animalsfood/assets/css/styles.css">
        <script type="text/javascript" src="http://localhost:8080/animalsfood/assets/lib/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost:8080/animalsfood/assets/lib/bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 header" style="">
                        <div class="text-center">
                            <h1>Animals Food</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>
   