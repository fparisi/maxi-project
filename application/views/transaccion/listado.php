
<script src="http://localhost:8080/animalsfood/assets/js/productos/alta.js" type="text/javascript"></script>
<?php
$this->load->view('home/menu');
?>
<?php if ($usuario): ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <?php if ($perfil == 1): ?>
                    <h1>Movimientos</h1>
                <?php elseif ($perfil == 2): ?>
                    <h1>Mis Compras</h1>
                <?php endif; ?>
                <h1></h1>
                <table class="table table-striped">
                    <?php if ($transacciones): ?>
                        <thead>
                            <tr>
                                <?php if ($perfil == 1): ?>
                                    <th>Usuario</th>
                                    <th>Rol</th>
                                <?php endif; ?>
                                <th>Producto</th>
                                <?php if ($perfil == 1): ?>
                                <th>Precio</th>
                                <?php else: ?>
                                <th>Valor total</th>
                                <?php endif; ?>
                                <th>Tipo accion</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transacciones as $transaccion): ?>
                                <tr>
                                    <?php if ($perfil == 1): ?>
                                        <th><?php echo $transaccion->email ?></th>
                                        <th><?php echo $transaccion->rol ?></th>
                                    <?php endif; ?>
                                    <td><?php echo $transaccion->producto ?></td>
                                     <?php if ($perfil == 1 && $transaccion->valor <=0 ): ?>
                                    <td><?php echo $transaccion->precio ?></td>
                                    <?php else: ?>
                                    <td><?php echo $transaccion->valor ?></td>
                                    <?php endif;?>
                                    <td><?php echo $transaccion->accion ?></td>
                                    <td><?php echo $transaccion->fecha ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <p class="alert alert-warning text-center">No hay resultados para la bùsqueda</p>
                    <?php endif; ?>
                </table>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
    </div>
<?php endif; ?>
    
