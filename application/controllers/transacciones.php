<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transacciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    
    public function alta() {
     
        $usuario = $this->session->userdata('id_usuario');
        $perfil = $this->session->userdata('id_perfil');
        $this->load->model('transaccion');
        // si es administardor se pasa el segundo parametro de la funcion en null para que traiga todos los movimientos
        if($perfil == 1){
             $transacciones = $this->transaccion->getListadoTransacciones($usuario, true); 
        }else {
             $transacciones = $this->transaccion->getListadoTransacciones($usuario); 
        }
       
        $this->load->view('templates/header');
        $this->load->view('transaccion/listado',array('transacciones' => $transacciones,'usuario' => $this->session->userdata('id_usuario'),
            'perfil' => $this->session->userdata('id_perfil'),
            'email' => $this->session->userdata('email')));
        $this->load->view('templates/footer');
     
    }

}