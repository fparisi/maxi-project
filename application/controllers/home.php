<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        
        if(!$this->session->userdata('id_usuario')){
            redirect("http://localhost:8080/animalsfood/");
        }
        $this->load->view('templates/header');
        $this->load->view('home/home');
        $this->load->view('templates/footer');
    }
    
    

}
