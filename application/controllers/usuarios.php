<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function alta() {

        $this->load->model('perfil');
        $perfiles = $this->perfil->findAll();
        $this->load->view('templates/header');
        $this->load->view('usuarios/alta', array('usuario' => $this->session->userdata('id_usuario'),
            'perfil' => $this->session->userdata('id_perfil'),
            'email' => $this->session->userdata('email'),
            'perfiles' => $perfiles));
        $this->load->view('templates/footer');
    }

    public function registrar_usuario() {

        //Recibir los datos del formulario que vienen por post, desde javaScript
        // guardamos en variables php , los datos que vienen por post, desde js
        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellido');
        $dni = $this->input->post('dni');
        $telefono = $this->input->post('telefono');
        $domicilio = $this->input->post('domicilio');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $perfil = $this->input->post('perfil') ? $this->input->post('perfil') : null; 
        
        // cargamos el modelo producto
        $this->load->model('usuario');
        //llamamos al metodo guardar_producto dentro del modelo producto
        // y le pasamos los datos que llegaron por post
        //primero verificamos que todos los campos esten completos antes de insertar en la base de datos
        if (empty($nombre) || empty($apellido) || empty($dni) || empty($telefono) || empty($domicilio) || empty($email)  || empty($password)) {
            echo"Debe completar todos los campos";
            return;
        } else {
            $this->usuario->guardar_usuario($nombre, $apellido, $dni, $telefono,$domicilio,$email,$password,$perfil);
            echo "el usuario se guardo correctamente";
        }
    }

    //esta accion se llama cuando se elije la opcion "modificar usuario" desde el menu
    public function modificar(){
        $this->load->view('templates/header');
        $this->load->model('usuario');
        $usuario_actual =  $this->usuario->find($this->session->userdata('id_usuario'));
        $this->load->view('usuarios/modificar',array(
            'usuario' => $this->session->userdata('id_usuario'),
            'usuario_actual' => $usuario_actual,
            'perfil' => $this->session->userdata('id_perfil'),
            'email' => $this->session->userdata('email')));
        $this->load->view('templates/footer');
    }
   
     public function modificar_usuario(){
        //Recibir los datos del formulario que vienen por post, desde javaScript
        // guardamos en variables php , los datos que vienen por post, desde js
         $id_user = $this->input->post('id_user');
        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellido');
        $dni = $this->input->post('dni');
        $telefono = $this->input->post('telefono');
        $domicilio = $this->input->post('domicilio');
        $email = $this->input->post('email');
        $password = $this->input->post('password') ? $this->input->post('password') : NULL;
        $perfil = $this->session->userdata('id_perfil'); 
        
        // cargamos el modelo producto
        $this->load->model('usuario');
        //llamamos al metodo guardar_producto dentro del modelo producto
        // y le pasamos los datos que llegaron por post
        //primero verificamos que todos los campos esten completos antes de insertar en la base de datos
        if (empty($nombre) || empty($apellido) || empty($dni) || empty($telefono) || empty($domicilio) || empty($email)) {
            echo"Debe completar todos los campos";
            return;
        } else {
            //si no se ingreso password
            if(!$password){
                //se ingresa el password que ya estaba en la base de datos para este usuario
                $usuario_logueado = $this->usuario->find($this->session->userdata('id_usuario'));
                $password = $usuario_logueado->password;
            }
            $this->usuario->editar_usuario($id_user,$nombre, $apellido, $dni, $telefono,$domicilio,$email,$password,$perfil);
            echo "el usuario se edito correctamente";
        } 
    }
}
