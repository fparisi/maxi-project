<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function alta() {

        $this->load->model('marca');
        $marcas = $this->marca->findAll();
        $this->load->view('templates/header');
        $this->load->view('productos/alta', array('marcas' => $marcas, 'usuario' => $this->session->userdata('id_usuario'),
            'perfil' => $this->session->userdata('id_perfil'),
            'email' => $this->session->userdata('email')));
        $this->load->view('templates/footer');
    }

    public function registrar_producto() {

        //Recibir los datos del formulario que vienen por post, desde javaScript
        // guardamos en variables php , los datos que vienen por post, desde js
        $descripcion = $this->input->post('descripcion') ? $this->input->post('descripcion') : null;
        $marca = $this->input->post('marca') ? $this->input->post('marca') : null;
        $stock = $this->input->post('stock') ? $this->input->post('stock') : null;
        $precio = $this->input->post('precio') ? $this->input->post('precio') : null;
        // cargamos el modelo producto
        $this->load->model('producto');
        //llamamos al metodo guardar_producto dentro del modelo producto
        // y le pasamos los datos que llegaron por post
        //primero verificamos que todos los campos esten completos antes de insertar en la base de datos
        if (empty($descripcion) || empty($marca) || empty($stock) || empty($precio)) {
            echo"Debe completar todos los campos";
            return;
        } else {
            $this->producto->guardar_producto($descripcion, $marca, $stock, $precio);
            echo "el producto se guardo correctamente";
        }
    }

    public function consultar() {
        //Se carga la vista de la consulta del producto 
        $this->load->view('templates/header');
        $this->load->view('productos/consultar', array('usuario' => $this->session->userdata('id_usuario'),
            'perfil' => $this->session->userdata('id_perfil'),
            'email' => $this->session->userdata('email')));
        $this->load->view('templates/footer');
    }

    public function consultar_producto() {
        //se entra a esta funcion por ajax, desde el metodo $.post del archivo productos/consultar.js  que apunta a esta accion
        // se recibe el dato descripcion por post
        $descripcion = $this->input->post('descripcion') ? $this->input->post('descripcion') : NULL; // nos aseguramos de que haya dato, o que sino sea null
        //var_dump($descripcion);
        //cargamos el modelo producto, para acceder a la tabla producto donde tenemos la informacion de nuestros productos guardados
        $this->load->model('producto');
        //si  llego descripcion en la peticion
        if ($descripcion) {
            //buscamos un producto con esa descripcion en particular
            $productos = $this->producto->get_producto($descripcion);
            //   $productos = $this->producto->getByDescription($descripcion);
        } else {
            // obtenemos todos los productos (por que no hay que buscar por una descripcion en particular) 
            $productos = $this->producto->get_productos();
        }
        //cargamos la vista del listado, y pasamos el array con los productos

        $perfil = $this->session->userdata('id_perfil');
        $this->load->view('productos/listadoProductos', array('productos' => $productos, 'perfil' => $perfil));
    }

    public function eliminar_producto() {

        $id_producto = $this->input->post('id_producto');
        // var_dump($id_producto);
        // die();
        $this->load->model('producto');
        $response = $this->producto->delete_by_id($id_producto);
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function comprar_producto() {
        $usuario = $this->session->userdata('id_usuario');
        $id_producto = $this->input->post('id_producto');
        $cantidad = (int) $this->input->post('cantidad');
       
        $this->load->model('producto');
        $producto = $this->producto->find($id_producto);
        
        if ($producto->stock > $cantidad) {
            $stock = $producto->stock - $cantidad;
            $producto->stock = $stock;
            $valor = $cantidad * $producto->precio;
            $response = $this->producto->compra_producto($producto, $usuario,$valor);
        }
        
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

}
