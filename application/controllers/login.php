<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function index() {
        //si el usuario no esta logueado, que cargue la vista de login
     //   if (!$this->usuario) {
            $this->load->view('templates/header');
            $this->load->view('login/alta');
            $this->load->view('templates/footer');
      //  } else { //sino que muestre la pantalla de inicio de la aplicacion
       //     redirect('home/index');
       // }
    }

    public function iniciar() {
        $response = $this->session->iniciar_sesion();
        //en response obtenemos la respuesta del metodo iniciar session
        // retornamos esa respuesta para que pueda ser evaluada desde javascript
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
        
    }

    public function salir() {
        $this->session->cerrar_sesion();
    }

}