<?php

class Usuario extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'usuario';
    }

    public function guardar_usuario($nombre, $apellido, $dni, $telefono, $domicilio, $email, $password, $perfil = null) {
        // creamos un array para hacer el insert en la base de datos
        // la "clave" del array son los nombres de los campos de la base de datos
        // el "valor" del array son las variables que llegan desde el controlador
        $datos = array(
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dni' => $dni,
            'telefono' => $telefono,
            'domicilio' => $domicilio,
            'email' => $email,
            'password' => $password
        );

        //el perfil se va a ingresar solo cuando un usuario administrador de de alta un usuario
        if ($perfil) {
            $datos['id_perfil'] = $perfil;
        } else {// si no se ingresa el perfil es por que se esta registrando un usuario nuevo, por lo tanto se guarda con el perfil 2 (cliente)
            $datos['id_perfil'] = 2;
        }

        $id = $this->insert($datos);
        return $id;
    }

    public function editar_usuario($id_user,$nombre, $apellido, $dni, $telefono, $domicilio, $email, $password, $perfil) {

        $datos = array(
            'nombre' => $nombre,
            'apellido' => $apellido,
            'dni' => $dni,
            'telefono' => $telefono,
            'domicilio' => $domicilio,
            'email' => $email,
            'password' => $password,
            'id_perfil' => $perfil
        );

        $this->db->where('id', $id_user);
        $this->db->update('usuario', $datos);
        
        
    }

}
