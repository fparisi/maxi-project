<?php

class Producto extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = "producto";
    }

    public function guardar_producto($descripcion, $marca, $stock, $precio) {
        // creamos un array para hacer el insert en la base de datos
        // la "clave" del array son los nombres de los campos de la base de datos
        // el "valor" del array son las variables que llegan desde el controlador
        $this->load->model('accion');
        $this->load->model('transaccion');
        $perfil = $this->session->userdata('id_perfil');
        $usuario = $this->session->userdata('id_usuario');
        $fecha = date('Y-m-d ',strtotime(date('Y-m-d')));


        $this->db->trans_start();
        $datos = array(
            'stock' => $stock,
            'precio' => $precio,
            'descripcion' => $descripcion,
            'id_marca' => $marca
        );

       $id_producto = $this->insert($datos);
       
       $datos_trans = array('id_usuario' => $usuario,
                                      'id_producto' => $id_producto,
                                      'fecha' => $fecha);
       if($perfil == 1){//si es administrador, la accion es "alta producto"
           $datos_trans['id_accion'] = 2 ; 
       }
       
       $this->table = "transaccion";
       $this->insert($datos_trans);
       
       $this->db->trans_complete();
       
       return ;
    }
    
    
    
    function get_productos(){
        $this->db->select('p.id id,p.stock stock,p.descripcion producto,p.precio precio,m.nombre marca');
        $this->db->from("{$this->table} p");
        $this->db->join('marca m','p.id_marca = m.id','INNER');
        return $this->get_objects($this->db->get());
    }
    
     function get_producto($descripcion){
        $this->db->select('p.id id,p.stock stock,p.descripcion producto,p.precio precio,m.nombre marca');
        $this->db->from("{$this->table} p");
        $this->db->join('marca m','p.id_marca = m.id','INNER');
        $this->db->like('p.descripcion',$descripcion,'after');
        return $this->get_objects($this->db->get());
    }
    
    function Compra_producto($producto,$usuario,$valor = null){
       
        $arrayProd = (array) $producto; 
        $fecha = date('Y-m-d ',strtotime(date('Y-m-d')));
        $this->db->trans_start();
        $this->update_by_id($producto->id, $arrayProd);
        
       $datos_trans = array('id_usuario' => $usuario,
                                      'id_producto' => $producto->id,
                                      'fecha' => $fecha,
                                      'id_Accion' => 1,
                                      'valor' => $valor );
       
       $this->table = "transaccion";
       $response = (bool) $this->insert($datos_trans);
       $this->db->trans_complete();
        return $response;
    }
    
  
   
}
