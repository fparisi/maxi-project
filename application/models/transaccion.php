<?php

class transaccion extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = "transaccion";
    }

    function getListadoTransacciones($id_usuario,$admin = false) {
        $select = "t.id id_trans,"
                . "u.id id_user,"
                . "p.id id_prod,"
                . "a.id id_acci,"
                . "u.email email,"
                . "pe.rol rol,"
                . "p.descripcion producto,"
                . "p.precio,"
                . "a.descripcion accion,"
                . "p.stock stock,"
                . "t.fecha,"
                . "t.valor valor ";
        
        $this->db->select($select);
        $this->db->from("{$this->table} t");
        $this->db->join("usuario u","t.id_usuario = u.id","INNER ");
        $this->db->join("perfil pe","pe.id = u.id_perfil","INNER ");
        $this->db->join("producto p","t.id_producto = p.id","INNER ");
        $this->db->join("accion a","t.id_accion = a.id","INNER ");
        if($admin == false){
            $this->db->where("u.id",$id_usuario);
        }
        return $this->get_objects($this->db->get());
    }

}
