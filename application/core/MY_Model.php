<?php

class MY_Model extends CI_Model {
    
    protected $table;
    
    public function set_table($table) {
        if (!empty($table) && is_string($table)) {
            $this->table = $table;
        }
    }

    public function insert($data) {
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }
    public function insertAll(array $data) {
        return $this->db->insert_batch($this->table, $data);
    }
    public function update_by_id($id, $data) {
        $this->db->where('id', $id);
        return $this->db->update($this->table, $data);
    }    
    
    public function update_by_field($field, $value, $data) {
        $this->db->where($field, $value);
        return $this->db->update($this->table, $data);
    }
    
    public function delete_by_id($id) {
        $this->db->where('id', $id);
        return $this->db->delete($this->table); 
    }
    
    public function get_object($query, $free_result = TRUE) {
        if ($query->num_rows() > 0) {
            $result = $query->result();
            if ($free_result) {
                $query->free_result();
            }
            return $result[0];
        } else {
            return FALSE;
        }        
    }
    
    public function get_objects($query, $free_result = TRUE) {
        if ($query->num_rows() > 0) {
            $result = $query->result();
            if ($free_result) {
                $query->free_result();
            }            
            return $result;
        } else {
            return FALSE;
        }
    }
    
    public function get($where = array()) {
        $this->db->select('*');
        $this->db->from($this->table);
        if (!empty($where)) {
            $this->db->where($where);
        }
        return $this->get_objects($this->db->get());
    }
    
    public function get_by_id($id, $where = array()) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        if (!empty($where)) {
            $this->db->where($where);
        }
        return $this->get_object($this->db->get());
    }
    
    public function get_max($field) {
        $this->db->select_max($field);
        $this->db->from($this->table);
        $result = $this->get_object($this->db->get());
        return (int) $result->{$field};
    }
    
    public function get_max_id() {
        $this->db->select_max('id');
        $this->db->from($this->table);
        $result = $this->get_object($this->db->get());
        return (int) $result->id;        
    }
    
    public function get_max_where($field, $where) {
        $this->db->select_max($field);
        $this->db->from($this->table);
        $this->db->where($where);
        $result = $this->get_object($this->db->get());
        return $result ? (int) $result->{$field} : FALSE;
    }
    
    public function get_max_id_where($where) {
        $this->db->select_max('id');
        $this->db->from($this->table);
        $this->db->where($where);
        $result = $this->get_object($this->db->get());
        return $result ? (int) $result->id : FALSE;
    }
    
    public function is_unique($field, $value) {
        $this->db->select('*');
        $this->db->from($this->table);
        if (is_string($value)) {
            $this->db->where("LOWER('{$field}')", strtolower($value));
        } else {
            $this->db->where($field, $value);
        }
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        $query->free_result();
        return $num_rows === 0;
    }

    /*
     * Metodos de busqueda como los de Doctrine
     */
    
    public function findAll() {
        $this->db->select('*');
        $this->db->from($this->table);
        $query = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return $result;        
    }

    public function find($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return count($result) > 0 ? $result[0] : NULL;
    }

    public function findBy(array $where) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    public function findOneBy(array $where) {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return count($result) > 0 ? $result[0] : NULL;
    }

}

