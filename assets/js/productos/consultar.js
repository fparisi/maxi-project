$(function () {
    let $frm = $('#frm-producto-consultar');
    submitFrmConsultar($frm);
    resetForm($frm);
});



function submitFrmConsultar($frm) {
    $frm.on('submit', (e) => {
        e.preventDefault();
        var descripcion = $('#descripcion').val();
        var params = {descripcion: descripcion}
        $('#divListadoConsulta').load('http://localhost:8080/animalsfood/productos/consultar_producto', params, (data) => {
            $('#panelListadoProductos').removeClass('hidden');
            comprar_producto();
            eliminar_producto();
            
        }, 'json');
    });
}


function resetForm($frm) {
    $frm.on('reset', function () {
        $('#panelListadoProductos').addClass('hidden');
    });
}

function comprar_producto() {
    $('[data-action="comprar-producto"]').on('click', function () {
      var id_producto =  $(this).closest('tr').children('td:eq(0)').text();
      var cantidad =  $(this).closest('tr').children('td:eq(4)').children('input').val();
      if (cantidad.length === 0){
            alert("Debe ingresar una cantidad");
          return;
      }
       $.post("http://localhost:8080/animalsfood/productos/comprar_producto",{id_producto:id_producto,cantidad:cantidad},function(data){
          if(data === true){
            alert("SE HA COMPRADO EL PRODUCTO CORRECTAMENTE");
            window.location.reload();
          }
          return;
      }).fail(function(){
          alert("EL STOCK NO ES SUFICIENTE, NO SE PUDO REALIZAR LA COMPRA DEL PRODUCTO");
      });
      
    });
}

function eliminar_producto(){
    $('[data-action="eliminar-producto"]').on('click', function () {
      var id_producto =  $(this).closest('tr').children('td:eq(0)').text();
    //  alert(id_producto);
      $.post("http://localhost:8080/animalsfood/productos/eliminar_producto",{id_producto:id_producto},function(data){
          if(data === true){
            alert("SE HA ELIMINADO EL PRODUCTO CORRECTAMENTE");
            window.location.reload();
          }
          return;
      }).fail(function(){
          alert("HA OCURRIDO UN ERROR EN LA BASE DE DATOS, NO SE PUDO ELIMINAR EL PRODUCTO");
      });
    });
}