//cuando se cargue todo el documetno html
$(function () {
    //llamo a la funcion registrar_producto para que este disponible, cuando surga el evento submit del form(cuando pongan "registrar")
    registrar_producto();

});


function registrar_producto() {
    //selecciono el formulario por su id, y ejecutamos una funcion cuando alguien haga clcik en registrar, que es el boton con el evento submit del form
    $('#frm-productos-alta').submit(function (e) {

        //detenemos el evento submit del formulario, por lo que antes de ir a la ruta del action, hacemos algunas validaciones
        e.preventDefault();

        //seleccionamos los campos del formulario
        // guardamos en variables de javascript, los valores de los campos del formulario que se va a enviar
        var descripcion = $('#descripcion').val(); // $('#id') selector por id de cualquier elemento de la pagina html
        var marca = $('#marca').val();
        var stock = $('#stock').val();
        var precio = $('#precio').val();

        // validamos los datos que llegan del formulario

        //primero verificamos que ningun campo este vacio
        // .length es una funcion de javascript que cuenta cuantos caracteres tiene un elemento 
        // ej: si escribo maxi en el campo descripcion y hago el .length, va a devolver 4
        if (descripcion.length == 0) {
            alert("Debe ingresar una descripcion");
            return;
        }

        if (marca.length == 0) {
            alert("Debe ingresar una marca");
            return;
        } else {
            // ahora verificamos que el value de la marca sea numerico
            if (isNaN(marca)) {// solo cuando no sea un numero entrara en este if
                alert("El valor de la marca debe ser numerico");
                return;
            }
        }

        if (stock.length == 0) {
            alert("Debe ingresar un valor para el stock");
            return;
        } else {
            // ahora verificamos que el campo precio sea numerico
            if (isNaN(stock)) {// solo cuando no sea un numero entrara en este if
                alert("El valor del stock debe ser numerico");
                return;
            }
        }

        if (precio.length == 0) {
            alert("Debe ingresar un precio para el producto");
            return;
        } else {
            // ahora verificamos que el campo precio sea numerico
            if (isNaN(precio)) { // solo cuando no sea un numero entrara en este if
                alert("El valor del precio debe ser numerico");
                return;
            }
        }

        //una vez que sabemos que van a llegar los datos, armamos los parametros que vamos a pasar
        var params = {descripcion: descripcion, // objeto con los datos que lleva el formulario
            marca: marca,
            stock: stock,
            precio: precio
        };


        // hacemos una peticion post, a la accion registar producto en el controlador productos,
        //En un principío este formulario se dirigia a esta accion dentro de ese controlador 
        // pero nosotros interceptamos el evento desde javascript, y controlamos los datos 
        // ahora hay que mandar esos datos a donde iban desde un principio, controlador productos, accion registrar_producto
        $.post("http://localhost:8080/animalsfood/productos/registrar_producto", params, function () {// enviamos los parametros, y cuando se envien se ejecutara una funcion
            alert("El producto se guardo correctamente");
            //limpio los campos del formulario
            $('#descripcion').val("");
            $('#marca').val("");
            $('#stock').val("");
            $('#precio').val("");
        });

    });
}

