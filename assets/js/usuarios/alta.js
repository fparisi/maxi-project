//cuando se cargue todo el documetno html
$(function () {
    //llamo a la funcion registrar_producto para que este disponible, cuando surga el evento submit del form(cuando pongan "registrar")
    registrar_usuario();
    $('#sinpermiso').addClass('hidden');
    

});


function registrar_usuario() {
    //selecciono el formulario por su id, y ejecutamos una funcion cuando alguien haga clcik en registrar, que es el boton con el evento submit del form
    $('#frm-usuarios-alta').submit(function (e) {

        //detenemos el evento submit del formulario, por lo que antes de ir a la ruta del action, hacemos algunas validaciones
        e.preventDefault();
        //seleccionamos los campos del formulario
        // guardamos en variables de javascript, los valores de los campos del formulario que se va a enviar
        var nombre = $('#nombre').val(); // $('#id') selector por id de cualquier elemento de la pagina html
        var apellido = $('#apellido').val();
        var dni = $('#dni').val();
        var telefono = $('#telefono').val();
        var domicilio = $('#domicilio').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var perfil = $('#perfil').val();
        // validamos los datos que llegan del formulario

        //primero verificamos que ningun campo este vacio
        // .length es una funcion de javascript que cuenta cuantos caracteres tiene un elemento 
        // ej: si escribo maxi en el campo nombre y hago el .length, va a devolver 4
        if (nombre.length == 0) {
            alert("Debe ingresar un nombre");
            return;
        }

        if (apellido.length == 0) {
            alert("Debe ingresar un apellido");
            return;
        }

        if (dni.length == 0) {
            alert("Debe ingresar un dni");
            return;
        } else {
            // ahora verificamos que el value del dni sea numerico
            if (isNaN(dni)) {// solo cuando no sea un numero entrara en este if
                alert("El valor del dni debe ser numerico");
                return;
            }
        }

        if (telefono.length == 0) {
            alert("Debe ingresar un telefono");
            return;
        } else {
            // ahora verificamos que el value del dni sea numerico
            if (isNaN(telefono)) {// solo cuando no sea un numero entrara en este if
                alert("El valor del telefono debe ser numerico");
                return;
            }
        }

        if (domicilio.length == 0) {
            alert("Debe ingresar un domicilio");
            return;
        }

        if (email.length == 0) {
            alert("Debe ingresar un email");
            return;
        }

        if (password.length == 0) {
            password = null;
        }


        //una vez que sabemos que van a llegar los datos, armamos los parametros que vamos a pasar
        var params = {
            nombre: nombre, // objeto con los datos que lleva el formulario
            apellido: apellido,
            dni: dni,
            telefono: telefono,
            domicilio: domicilio,
            email: email,
            password: password,
            perfil: perfil
        };


        // hacemos una peticion post, a la accion registar producto en el controlador productos,
        //En un principío este formulario se dirigia a esta accion dentro de ese controlador 
        // pero nosotros interceptamos el evento desde javascript, y controlamos los datos 
        // ahora hay que mandar esos datos a donde iban desde un principio, controlador productos, accion registrar_producto
        $.post("http://localhost:8080/animalsfood/usuarios/registrar_usuario", params, function () {// enviamos los parametros, y cuando se envien se ejecutara una funcion
            alert("El usuario se guardo correctamente");
            //limpio los campos del formulario
            $('#nombre').val(''); // $('#id') selector por id de cualquier elemento de la pagina html
            $('#apellido').val('');
            $('#dni').val('');
            $('#telefono').val('');
            $('#domicilio').val('');
            $('#email').val('');
            $('#password').val('');
            $('#perfil').val('');
            
            window.location.href = "http://localhost:8080/animalsfood/home/index";
        });

    });
}

