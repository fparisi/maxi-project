$(function () {
    var $frm = $('#frm-iniciar-sesion');
    submitFrmIniciarSesion($frm);
});


function submitFrmIniciarSesion($frm) {
    $frm.on('submit', function (e) {
        e.preventDefault();
        
        var usuario = $('#usuario').val();
        
        var password = $('#password').val();
      //  alert(usuario);
        
        if (usuario.length == 0) {
            alert("Debe ingresar un usuario");
            return;
        }

        if (password.length == 0) {
            alert("Debe ingresar una password");
            return;
        }
        
        var params = {
            usuario: usuario,
            password: password
        }
        
      //  console.log(params);
        
       // hacemos la peticion post a la accion iniciar dentro del controlador login
        $.post("http://localhost:8080/animalsfood/login/iniciar", params, function(data) {// en data se guardara la respuesta que devuelve la accion iniciar
            if(data.success == true){// si devolvio succes true, quiere decir que el usuario se logueo bien
                window.location.href = "http://localhost:8080/animalsfood/home/index"; // entonces lo redirigimos a la pagina de inicio de la aplicacion
            }else{
                alert(data.errorMsg); // si decolvio succes true, tambien va a devolver un texto en errorMsg (ver MY_session.php)
            }
        },'json');
    });
}